# Python3 program to implement traveling salesman
# problem using naive approach.
from itertools import permutations
from sys import maxsize
import json


# implementation of traveling Salesman Problem
def travellingSalesmanProblem(graph, s, f, V):
    # store all vertex apart from source vertex
    vertex = []
    for i in range(V):
        if i != s:
            vertex.append(i)

            # store minimum weight Hamiltonian Cycle

    min_path = maxsize
    vertex_path = []

    next_permutation = permutations(vertex)
    for i in next_permutation:

        # store current Path weight(cost)
        current_pathweight = 0
        current_vertex_path = []

        # compute current path weight
        k = s
        for j in i:
            if graph[k][j] == 2147483647:
                current_pathweight = 2147483647
                break
            current_pathweight += graph[k][j]
            current_vertex_path.append(j)
            k = j
            if k == f:
                break

        # update minimum
        if(current_pathweight< min_path):
            vertex_path = current_vertex_path
        min_path = min(min_path, current_pathweight)

    return vertex_path


# Driver Code
if __name__ == "__main__":
    # matrix representation of graph

    f = open('test.json', )
    station_id = json.load(f)["uuids"]

    f = open('test.json', )
    graph = json.load(f)["matrix"]
    s = 13 #беляево
    f = 21 #калужская

    print(travellingSalesmanProblem(graph, s, f, len(graph)))
    print(station_id[s])
    print(station_id[f])

