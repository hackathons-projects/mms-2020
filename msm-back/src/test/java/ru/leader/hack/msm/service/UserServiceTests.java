package ru.leader.hack.msm.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import ru.leader.hack.msm.configuration.security.JwtTokenProvider;
import ru.leader.hack.msm.domain.model.UserEntity;
import ru.leader.hack.msm.domain.repository.UserRepository;
import ru.leader.hack.msm.exception.CustomException;
import ru.leader.hack.msm.model.UserDto;
import ru.leader.hack.msm.model.request.UserRegistrationRequest;

import java.util.List;

@SpringBootTest
public class UserServiceTests extends AbstractServiceTests {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Test
    public void createUsers() {
        UserRegistrationRequest userRegistrationRequest = createTestUser();
        userService.signUp(userRegistrationRequest);

        Assertions.assertTrue(userRepository.existsByUsername("test"));
        Assertions.assertEquals("test", userRepository.findByUsername("test").orElseThrow(RuntimeException::new).getNickname());
        Assertions.assertEquals("123", userRepository.findByUsername("test").orElseThrow(RuntimeException::new).getInn());

        userService.delete("test");
    }

    @Test
    public void createExistUser() {
        UserRegistrationRequest userRegistrationRequest = createTestUser();
        userService.signUp(userRegistrationRequest);

        Assertions.assertTrue(userRepository.existsByUsername("test"));
        Assertions.assertEquals("test", userRepository.findByUsername("test").orElseThrow(RuntimeException::new).getNickname());

        final UserRegistrationRequest duplicate = createTestUser();
        CustomException customException = Assertions.assertThrows(CustomException.class, () -> userService.signUp(duplicate));

        Assertions.assertEquals("Username is already in use", customException.getMessage());

        userService.delete("test");
    }

    @Test
    public void authorizeTest() {
        UserRegistrationRequest userRegistrationRequest = createTestUser();
        userService.signUp(userRegistrationRequest);

        UserDto test = userService.search("test");
        Assertions.assertEquals("test", test.getNickname());

        UserEntity testEntity = userService.findByName("test");
        Assertions.assertEquals("test", testEntity.getUsername());

        String token = userService.signIn("test", "test");
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
        mockHttpServletRequest.addHeader("Authorization", "Bearer " + token);
        UserDto userDto = userService.whoAmI(mockHttpServletRequest);
        Assertions.assertEquals("test", userDto.getUsername());

        token = userService.refresh("test");
        mockHttpServletRequest = new MockHttpServletRequest();
        mockHttpServletRequest.addHeader("Authorization", "Bearer " + token);
        userDto = userService.whoAmI(mockHttpServletRequest);
        Assertions.assertEquals("test", userDto.getUsername());

        List<UserDto> all = userService.findAll();
        Assertions.assertEquals(3, all.size());
        Assertions.assertEquals("+7-000-000-00-00", all.stream().filter(userDto1 -> userDto1.getUsername().equals("test")).findFirst().orElseThrow().getPhoneNumber());

        userService.delete("test");
    }

    @Test
    public void notAuthorized() {
        UserRegistrationRequest userRegistrationRequest = createTestUser();
        userService.signUp(userRegistrationRequest);

        CustomException customException = Assertions.assertThrows(CustomException.class, () -> userService.signIn("123", "123"));
        Assertions.assertEquals("User not found", customException.getMessage());

        customException = Assertions.assertThrows(CustomException.class, () -> userService.signIn("test", "123"));
        Assertions.assertEquals("Not authorized", customException.getMessage());

        userService.delete("test");
    }

}
