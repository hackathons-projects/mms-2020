package ru.leader.hack.msm.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leader.hack.msm.domain.repository.LineRepository;
import ru.leader.hack.msm.model.LineDto;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Transactional
@Service
public class LinesService {

    private final ModelMapper mapper;
    private final LineRepository lineRepository;

    public List<LineDto> getAllLines() {
        return lineRepository.findAll().parallelStream().map(lineEntity -> mapper.map(lineEntity, LineDto.class)).collect(Collectors.toList());
    }

    public LineDto getLineByTitle(String title) {
        return mapper.map(lineRepository.findByTitle(title).orElseThrow(), LineDto.class);
    }

    public LineDto getLineById(UUID lineId) {
        return mapper.map(lineRepository.findByLineId(lineId).orElseThrow(), LineDto.class);
    }

    public LineDto getLineByNumber(String number) {
        return mapper.map(lineRepository.findByNumber(number).orElseThrow(), LineDto.class);
    }
}
