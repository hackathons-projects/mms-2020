package ru.leader.hack.msm.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leader.hack.msm.domain.repository.StationRepository;
import ru.leader.hack.msm.model.StationDto;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Transactional
@Service
public class StationService {

    private final UserService userService;
    private final StationRepository stationRepository;
    private final ModelMapper mapper;

    public List<StationDto> getAll() {
        return stationRepository.findAll().parallelStream().map(station -> mapper.map(station, StationDto.class)).collect(Collectors.toList());
    }

    public StationDto findByStationId(UUID stationId) {
        return mapper.map(stationRepository.findByStationId(stationId).orElseThrow(), StationDto.class);
    }
}
