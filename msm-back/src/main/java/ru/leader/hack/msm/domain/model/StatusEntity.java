package ru.leader.hack.msm.domain.model;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "statuses")
public class StatusEntity extends BaseEntity<Long> {

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private String iconId;
}
