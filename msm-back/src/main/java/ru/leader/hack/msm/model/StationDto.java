package ru.leader.hack.msm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.leader.hack.msm.domain.model.StationType;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StationDto {

    private UUID stationId;

    private UUID lineId;

    private String status;

    private StationType type;

    private String title;

    private String openTime;

    private String closeTime;

    private Boolean disabledPeopleAccepted;

    private Boolean interceptParking;

    private Boolean railStation;

    private Boolean airport;

    private Boolean autoStation;

    private Integer exitCount;
}
