package ru.leader.hack.msm.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.leader.hack.msm.domain.model.RelationEntity;
import ru.leader.hack.msm.domain.model.StationEntity;

import java.util.List;
import java.util.UUID;

public interface RelationRepository extends JpaRepository<RelationEntity, Long> {

    List<RelationEntity> findAllBySourceInAndTargetIn(List<StationEntity> source, List<StationEntity> target);

    RelationEntity findBySource_StationIdAndTarget_StationId(UUID source_stationId, UUID target_stationId);
}
