package ru.leader.hack.msm.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Audited
@Entity
@Table(name = "msm_users")
public class UserEntity extends BaseEntity<Long> {

    @Column(unique = true, nullable = false)
    private String username;

    @Column
    private String nickname;

    @Column
    private String email;

    @Column
    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(unique = true, nullable = false)
    private String phoneNumber;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String surname;

    @Column
    private String inn;

    @Column
    private String passportNumber;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column
    private LocalDate birthDay;

    @Column
    private String avatarId;
}
