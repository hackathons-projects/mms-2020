package ru.leader.hack.msm.domain.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, exclude = {"relations", "stationLine", "status"})
@ToString(callSuper = true, exclude = {"relations", "stationLine", "status"})
@Entity
@Table(name = "stations")
public class StationEntity extends BaseEntity<Long> {

    @Column
    private UUID stationId;

    @OneToMany(mappedBy = "source", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<RelationEntity> relations = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "line_id")
    private LineEntity stationLine;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private StatusEntity status;

    @Enumerated(EnumType.STRING)
    private StationType type;

    @Column
    private String title;

    @Column
    private String openTime;

    @Column
    private String closeTime;

    @Column
    private Boolean disabledPeopleAccepted;

    @Column
    private Boolean interceptParking;

    @Column
    private Boolean railStation;

    @Column
    private Boolean airport;

    @Column
    private Boolean autoStation;

    @Column
    private Integer exitCount;
}
