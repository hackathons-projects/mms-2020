package ru.leader.hack.msm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.leader.hack.msm.model.LineDto;
import ru.leader.hack.msm.service.LinesService;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/lines")
public class LinesController {

    private final LinesService linesService;

    @GetMapping
    public List<LineDto> getAllLines() {
        return linesService.getAllLines();
    }

    @GetMapping("/title/{title}")
    public LineDto getByTitle(@PathVariable String title) {
        return linesService.getLineByTitle(title);
    }

    @GetMapping("/lineId/{uuid}")
    public LineDto getByUuid(@PathVariable UUID uuid) {
        return linesService.getLineById(uuid);
    }

    @GetMapping("/number/{number}")
    public LineDto getByNumber(@PathVariable String number) {
        return linesService.getLineByNumber(number);
    }

}
