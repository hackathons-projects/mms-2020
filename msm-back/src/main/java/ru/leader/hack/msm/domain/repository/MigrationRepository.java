package ru.leader.hack.msm.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.leader.hack.msm.domain.model.MigrationEntity;

public interface MigrationRepository extends JpaRepository<MigrationEntity, Long> {

    boolean existsByClassName(String className);
}
