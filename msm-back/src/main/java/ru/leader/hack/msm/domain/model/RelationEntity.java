package ru.leader.hack.msm.domain.model;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, exclude = {"source", "target", "route"})
@ToString(callSuper = true, exclude = {"source", "target", "route"})
@Entity
@Table(name = "relations")
public class RelationEntity extends BaseEntity<Long> {

    @ManyToOne
    @JoinColumn(name = "source_station_id")
    private StationEntity source;

    @ManyToOne
    @JoinColumn(name = "target_station_id")
    private StationEntity target;

    @Column
    private Long transferTime;

    @Enumerated(EnumType.STRING)
    private TransferType transferType;

    @ManyToOne
    @JoinColumn(name = "route_id")
    private RouteEntity route;
}
