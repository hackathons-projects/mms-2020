package ru.leader.hack.msm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.leader.hack.msm.model.StationDto;
import ru.leader.hack.msm.service.StationService;
import ru.leader.hack.msm.service.UserService;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/stations")
public class StationController {

    private final StationService stationService;
    private final UserService userService;

    @GetMapping
    public List<StationDto> getAll() {
        return stationService.getAll();
    }

    @GetMapping("/{stationId}")
    public StationDto findByStationId(@PathVariable UUID stationId) {
        return stationService.findByStationId(stationId);
    }

}
