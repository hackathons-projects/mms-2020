package ru.leader.hack.msm.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.leader.hack.msm.domain.model.StationEntity;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface StationRepository extends JpaRepository<StationEntity, Long> {

    Optional<StationEntity> findByStationId(UUID stationId);

    List<StationEntity> findAllByStationLine_LineIdIn(Set<UUID> stationId);
}
