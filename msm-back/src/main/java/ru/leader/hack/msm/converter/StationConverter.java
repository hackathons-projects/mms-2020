package ru.leader.hack.msm.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.leader.hack.msm.domain.model.LineEntity;
import ru.leader.hack.msm.domain.model.StationEntity;
import ru.leader.hack.msm.domain.model.StatusEntity;
import ru.leader.hack.msm.model.StationDto;

import java.util.Optional;

@Transactional
@Component
public class StationConverter implements Converter<StationEntity, StationDto> {

    @Override
    public StationDto convert(MappingContext<StationEntity, StationDto> mappingContext) {
        StationDto stationDto = Optional.ofNullable(mappingContext.getDestination()).orElse(new StationDto());
        stationDto.setAirport(mappingContext.getSource().getAirport());
        stationDto.setAutoStation(mappingContext.getSource().getAutoStation());
        stationDto.setCloseTime(mappingContext.getSource().getCloseTime());
        stationDto.setDisabledPeopleAccepted(mappingContext.getSource().getDisabledPeopleAccepted());
        stationDto.setExitCount(mappingContext.getSource().getExitCount());
        stationDto.setInterceptParking(mappingContext.getSource().getInterceptParking());
        stationDto.setLineId(Optional.ofNullable(mappingContext.getSource().getStationLine()).map(LineEntity::getLineId).orElse(null));
        stationDto.setOpenTime(mappingContext.getSource().getOpenTime());
        stationDto.setRailStation(mappingContext.getSource().getRailStation());
        stationDto.setStationId(mappingContext.getSource().getStationId());
        stationDto.setStatus(Optional.ofNullable(mappingContext.getSource().getStatus()).map(StatusEntity::getTitle).orElse(null));
        stationDto.setTitle(mappingContext.getSource().getTitle());
        stationDto.setType(mappingContext.getSource().getType());
        return stationDto;
    }
}
