package ru.leader.hack.msm.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.leader.hack.msm.domain.model.LineEntity;

import java.util.Optional;
import java.util.UUID;

public interface LineRepository extends JpaRepository<LineEntity, Long> {

    Optional<LineEntity> findByLineId(UUID lineId);

    Optional<LineEntity> findByTitle(String title);

    Optional<LineEntity> findByNumber(String number);
}
