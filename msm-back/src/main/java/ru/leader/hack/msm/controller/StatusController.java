package ru.leader.hack.msm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.leader.hack.msm.model.StatusDto;
import ru.leader.hack.msm.service.StatusService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/statuses")
public class StatusController {

    private final StatusService statusService;

    @GetMapping
    public List<StatusDto> getAll(){
        return statusService.getAll();
    }

}
