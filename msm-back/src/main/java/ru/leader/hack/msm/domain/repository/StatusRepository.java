package ru.leader.hack.msm.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.leader.hack.msm.domain.model.StatusEntity;

import java.util.Optional;
import java.util.UUID;

public interface StatusRepository extends JpaRepository<StatusEntity, Long> {

    Optional<StatusEntity> findByTitle(String title);
}
