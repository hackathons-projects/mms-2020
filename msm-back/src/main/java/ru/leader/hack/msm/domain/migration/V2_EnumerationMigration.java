package ru.leader.hack.msm.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.leader.hack.msm.domain.model.*;
import ru.leader.hack.msm.domain.repository.LineRepository;
import ru.leader.hack.msm.domain.repository.StationRepository;
import ru.leader.hack.msm.domain.repository.StatusRepository;

import java.util.HashMap;
import java.util.HashSet;

@RequiredArgsConstructor
@Transactional
@Component
public class V2_EnumerationMigration implements Migration {

    private final StatusRepository statusRepository;

    @Override
    public String getId() {
        return V2_EnumerationMigration.class.getName();
    }

    @Override
    public void migrate() {
        statusRepository.save(new StatusEntity("ACTIVE", "Работает", null));
        statusRepository.save(new StatusEntity("CLOSED", "Закрыта на вход", null));
        statusRepository.save(new StatusEntity("CLOSED_FOR_EXIT", "Закрыта на выход", null));
        statusRepository.save(new StatusEntity("REPAIR", "На ремонте", null));
        statusRepository.save(new StatusEntity("CROWDED", "Большое скопление людей", null));
        statusRepository.save(new StatusEntity("DISCOUNT", "Сниженная стоимость", null));
//        test();
    }

//    private void test() {
//        StationEntity saved1 = stationRepository.save(new StationEntity(
//                new HashMap<>(),
//                new HashSet<>() {{
//                    add(new LineEntity("Круг ада", 666));
//                }},
//                statusRepository.findByTitle("ACTIVE").orElseThrow(),
//                StationType.MCC,
//                "Ололошная 1",
//                "Ни свет ни заря",
//                "Когда мертвый петух клюнет",
//                true,
//                true,
//                true,
//                true,
//                true,
//                1
//        ));
//
//        StationEntity saved2 = stationRepository.save(new StationEntity(
//                new HashMap<>(),
//                new HashSet<>() {{
//                    add(new LineEntity("Круг ада", 666));
//                }},
//                statusRepository.findByTitle("ACTIVE").orElseThrow(),
//                StationType.MCC,
//                "Омск 1",
//                "Скоро рассвет",
//                "Выхода нет",
//                true,
//                true,
//                true,
//                true,
//                true,
//                0
//        ));
//
//        saved1.getRelations().put(lineRepository.findByTitle("Круг ада").orElseThrow().getId(),
//                new RelationEntity(saved1, saved2, 666L, TransferType.PEDESTRIAN));
//        stationRepository.save(saved1);
//    }
}
