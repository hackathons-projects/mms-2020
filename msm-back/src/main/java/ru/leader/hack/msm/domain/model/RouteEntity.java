package ru.leader.hack.msm.domain.model;


import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, exclude = {"routeLine"})
@ToString(callSuper = true, exclude = {"routeLine"})
@Entity
@Table(name = "routes")
public class RouteEntity extends BaseEntity<Long> {

    @Column
    private UUID routeId;

    @ManyToOne
    @JoinColumn(name = "line_id")
    private LineEntity routeLine;
}
