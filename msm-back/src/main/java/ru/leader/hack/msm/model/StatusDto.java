package ru.leader.hack.msm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StatusDto {

    private String title;

    private String description;

    private String iconId;
}
