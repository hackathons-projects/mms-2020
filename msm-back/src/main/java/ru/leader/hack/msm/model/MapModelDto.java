package ru.leader.hack.msm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class MapModelDto {
    private List<UUID> uuids = new ArrayList<>();
    private Integer[][] matrix;
}
