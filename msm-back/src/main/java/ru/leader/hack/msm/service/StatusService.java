package ru.leader.hack.msm.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leader.hack.msm.domain.repository.StatusRepository;
import ru.leader.hack.msm.model.StatusDto;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Transactional
@Service
public class StatusService {

    private final ModelMapper mapper;
    private final StatusRepository statusRepository;

    public List<StatusDto> getAll() {
        return statusRepository.findAll().parallelStream().map(statusEntity -> mapper.map(statusEntity, StatusDto.class)).collect(Collectors.toList());
    }
}
