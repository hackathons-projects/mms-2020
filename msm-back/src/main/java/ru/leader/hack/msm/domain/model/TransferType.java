package ru.leader.hack.msm.domain.model;

public enum TransferType {
    PEDESTRIAN, RAILWAY
}
