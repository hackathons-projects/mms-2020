package ru.leader.hack.msm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.leader.hack.msm.model.MapModelDto;
import ru.leader.hack.msm.model.StationDto;
import ru.leader.hack.msm.model.request.ModelStationRequest;
import ru.leader.hack.msm.service.MapService;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/map")
public class MapController {

    private final MapService mapService;

    @GetMapping("/{start}/{finish}")
    public List<StationDto> getPath(@PathVariable UUID start, @PathVariable UUID finish) {
        return mapService.getPath(start, finish);
    }

    @GetMapping
    public MapModelDto getMapModel() {
        return mapService.getModel();
    }

    @PostMapping("/byLineIds")
    public MapModelDto generateModelByStationIds(@RequestBody ModelStationRequest modelRequest) {
        return mapService.getModelByLineIds(modelRequest.getLineIds());
    }

    @GetMapping("/test/{start}/{finish}")
    public List<StationDto> getTestUUID(@PathVariable UUID start, @PathVariable UUID finish) {
        return mapService.get(start, finish);
    }
}
