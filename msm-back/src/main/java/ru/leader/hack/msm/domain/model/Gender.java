package ru.leader.hack.msm.domain.model;

public enum Gender {
    MALE, FEMALE
}
