package ru.leader.hack.msm.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.leader.hack.msm.domain.model.LineEntity;
import ru.leader.hack.msm.domain.model.RouteEntity;
import ru.leader.hack.msm.domain.model.StationEntity;
import ru.leader.hack.msm.model.LineDto;

import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Component
public class LineConverter implements Converter<LineEntity, LineDto> {

    @Override
    public LineDto convert(MappingContext<LineEntity, LineDto> mappingContext) {
        LineDto lineDto = Optional.ofNullable(mappingContext.getDestination()).orElse(new LineDto());
        lineDto.setLineId(mappingContext.getSource().getLineId());
        lineDto.setNumber(mappingContext.getSource().getNumber());
        lineDto.setTitle(mappingContext.getSource().getTitle());
        lineDto.setStationIds(
                Optional.ofNullable(mappingContext.getSource().getStations())
                        .map(stationEntities -> stationEntities.parallelStream().map(StationEntity::getStationId).collect(Collectors.toSet()))
                        .orElse(new HashSet<>())
        );
        lineDto.setRouteIds(
                Optional.ofNullable(mappingContext.getSource().getRoutes())
                        .map(stationEntities -> stationEntities.parallelStream().map(RouteEntity::getRouteId).collect(Collectors.toSet()))
                        .orElse(new HashSet<>())
        );
        return lineDto;
    }
}
