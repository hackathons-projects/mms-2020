package ru.leader.hack.msm.model;

import lombok.NoArgsConstructor;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@NoArgsConstructor
public class TspResult {
    private AtomicLong atomicLong = new AtomicLong(Long.MAX_VALUE);
    private List<Integer> path;

    public AtomicLong getAtomicLong() {
        return atomicLong;
    }

    public synchronized List<Integer> getPath() {
        return path;
    }

    public synchronized void set(List<Integer> path, Long w) {
        this.atomicLong.set(w);
        this.path = path;
    }
}
