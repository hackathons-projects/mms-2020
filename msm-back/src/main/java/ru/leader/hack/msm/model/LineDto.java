package ru.leader.hack.msm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LineDto {

    private UUID lineId;

    private Set<UUID> stationIds;

    private Set<UUID> routeIds;

    private String title;

    private String number;
}
