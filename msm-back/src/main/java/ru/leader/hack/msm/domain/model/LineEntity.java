package ru.leader.hack.msm.domain.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, exclude = {"stations", "routes"})
@ToString(callSuper = true, exclude = {"stations", "routes"})
@Entity
@Table(name = "lines")
public class LineEntity extends BaseEntity<Long> {

    @Column
    private UUID lineId;

    @OneToMany(mappedBy = "stationLine", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<StationEntity> stations = new HashSet<>();

    @OneToMany(mappedBy = "routeLine", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<RouteEntity> routes = new HashSet<>();

    @Column
    private String title;

    @Column
    private String number;
}
