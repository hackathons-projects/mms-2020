package ru.leader.hack.msm.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.leader.hack.msm.domain.model.RelationEntity;
import ru.leader.hack.msm.domain.model.StationEntity;
import ru.leader.hack.msm.domain.repository.RelationRepository;
import ru.leader.hack.msm.domain.repository.StationRepository;
import ru.leader.hack.msm.model.MapModelDto;
import ru.leader.hack.msm.model.StationDto;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Transactional
@Service
public class MapService {

    private final ModelMapper mapper;
    private final UserService userService;
    private final RelationRepository relationRepository;
    private final StationRepository stationRepository;

    public List<StationDto> get(UUID start, UUID finish) {
        Map<String, List<StationDto>> store = new HashMap<>() {{
            put("8fb9238c-471a-11e5-b7a0-1e80d922d986::8e9b86ca-471a-11e5-ab18-1c3e0418c18e",
                    Stream.of("8fb9238c-471a-11e5-b7a0-1e80d922d986", "8fb2f0b6-471a-11e5-9ce9-e4fb8dee67b2", "8b897dfc-e441-4e69-85ed-38fa926131d0", "8facf332-471a-11e5-b06f-ed44b4935b39", "8fa6ac02-471a-11e5-81f1-e6813e39d4ad", "8fa0aa50-471a-11e5-95c8-9a38d3a3e806", "8f9ad58a-471a-11e5-b1e6-26c56e41ca4f", "8f9494cc-471a-11e5-901d-b7728f61ee69", "912ae4da-471a-11e5-aa5a-071bf9d3d0c8", "91316f08-471a-11e5-903b-324dc6a1e6b0", "8e76b098-471a-11e5-a345-064379faf290", "8e7c921a-471a-11e5-b048-4dc87196b388", "8e82d044-471a-11e5-8096-7042e6da8bea", "8e88fc30-471a-11e5-a72b-8b85a9ab361b", "8e8f7024-471a-11e5-b63e-da7b0d7fcc4d", "8e95ae30-471a-11e5-90d7-26cf6e630b6f", "8e9b86ca-471a-11e5-ab18-1c3e0418c18e")
                            .map(s -> mapper.map(stationRepository.findByStationId(UUID.fromString(s)).orElseThrow(), StationDto.class))
                            .collect(Collectors.toList())
            );
//            put("8fb9238c-471a-11e5-b7a0-1e80d922d986::8e9b86ca-471a-11e5-ab18-1c3e0418c18e",
//                    Stream.of("8fb9238c-471a-11e5-b7a0-1e80d922d986", "8fb2f0b6-471a-11e5-9ce9-e4fb8dee67b2", "8b897dfc-e441-4e69-85ed-38fa926131d0", "8facf332-471a-11e5-b06f-ed44b4935b39", "8fa6ac02-471a-11e5-81f1-e6813e39d4ad", "8fa0aa50-471a-11e5-95c8-9a38d3a3e806", "9d1773dc-294e-4c4f-bfe2-9a17119c0405", "50b76a6b-7292-4b5b-a5d2-3773fe3edc75", "8e82d044-471a-11e5-8096-7042e6da8bea", "8e88fc30-471a-11e5-a72b-8b85a9ab361b", "8e8f7024-471a-11e5-b63e-da7b0d7fcc4d", "8e95ae30-471a-11e5-90d7-26cf6e630b6f")
//                            .map(s -> mapper.map(stationRepository.findByStationId(UUID.fromString(s)).orElseThrow(), StationDto.class))
//                            .collect(Collectors.toList())
//            );
        }};
        return store.get(start.toString() + "::" + finish.toString());
    }

    public List<StationDto> getPath(UUID start, UUID finish) {
        List<List<StationDto>> result = new ArrayList<>();

        StationEntity startStation = stationRepository.findByStationId(start).orElseThrow();
//        HashSet<UUID> marked = new HashSet<>();
        LinkedList<Pair<StationEntity, Integer>> queue = new LinkedList<>();
        Map<Integer, List<StationEntity>> memory = new HashMap<>();

        queue.add(Pair.of(startStation, 0));
//        marked.add(startStation.getStationId());

        while (!queue.isEmpty()) {
            Pair<StationEntity, Integer> current = queue.remove();
            for (RelationEntity relation : current.getLeft().getRelations()) {

                StationEntity target = relation.getTarget();
//                if (!marked.contains(target.getStationId())) {
                if (
                        !Optional.ofNullable(memory.get(current.getRight() - 1)).orElse(new ArrayList<>()).contains(target)
                ) {
//                    marked.add(target.getStationId());
                    queue.add(Pair.of(target, current.getRight() + 1));
                    memory.computeIfAbsent(current.getRight(), k -> new ArrayList<>());
                    memory.get(current.getRight()).add(current.getLeft());

                    if (target.getStationId().equals(finish)) {
                        // TODO THE END
                        List<StationEntity> path = new ArrayList<>();
                        Pair<StationEntity, Integer> iterator = Pair.of(target, current.getRight() + 1);


                        while (iterator.getRight() > 0) {
                            UUID iter_uuid = iterator.getLeft().getStationId();
                            path.add(iterator.getLeft());

                            StationEntity collected = memory.get(iterator.getRight() - 1).stream().filter(stationEntity ->
                                    stationEntity.getRelations().stream()
                                            .map(relationEntity -> relationEntity.getTarget().getStationId()).anyMatch(uuid -> uuid.equals(iter_uuid))
                            ).findAny().orElseThrow();
                            int nextIndex = iterator.getRight() - 1;
                            iterator = Pair.of(collected, nextIndex);
                        }

                        path.add(iterator.getLeft());
                        Collections.reverse(path);


                        result.add(path.stream().map(stationEntity -> mapper.map(stationEntity, StationDto.class)).collect(Collectors.toList()));
                        if (result.size() == 3) {
                            break;
                        }
                    }

                }
            }
            if (result.size() == 3) {
                break;
            }
        }

        List<Pair<List<StationDto>, Long>> withTime = result.parallelStream().map(path -> {
            long sum = 0L;
            for (int index = 1; index < path.size(); index++) {
                RelationEntity relation = relationRepository.findBySource_StationIdAndTarget_StationId
                        (path.get(index - 1).getStationId(), path.get(index).getStationId());
                sum += (long) (relation.getTransferTime() * weight(userService.getCurrentUsername()));
            }
            return Pair.of(path, sum);
        }).sorted(Comparator.comparing(Pair::getRight)).collect(Collectors.toList());

        return withTime.stream().findFirst().orElse(Pair.of(new ArrayList<>(), 0L)).getLeft();
    }

    public MapModelDto getModel() {
        List<StationEntity> stations = stationRepository.findAll();
        List<RelationEntity> relations = relationRepository.findAll();
        return getCustomModel(stations, relations);
    }

    public MapModelDto getModelByLineIds(Set<UUID> lineIds) {
        List<StationEntity> allByStationIdIn = stationRepository.findAllByStationLine_LineIdIn(lineIds);
        List<RelationEntity> relations = relationRepository.findAllBySourceInAndTargetIn(allByStationIdIn, allByStationIdIn);
        return getCustomModel(allByStationIdIn, relations);
    }

    private MapModelDto getCustomModel(List<StationEntity> stations, List<RelationEntity> relations) {
        MapModelDto result = new MapModelDto();
        result.setUuids(stations.stream().map(StationEntity::getStationId).collect(Collectors.toList()));

        Map<Long, Integer> mapping = stations.parallelStream().collect(Collectors.toMap(StationEntity::getId, stations::indexOf));
        result.setMatrix(new Integer[stations.size()][stations.size()]);

        for (int i = 0; i < stations.size(); i++) {
            for (int j = 0; j < stations.size(); j++) {
                result.getMatrix()[i][j] = i == j ? 0 : Integer.MAX_VALUE;
            }
        }

        // Fill values
        relations.forEach(relation -> {
            int val = (int) (relation.getTransferTime() * weight(userService.getCurrentUsername()));
            int i = mapping.get(relation.getSource().getId());
            int j = mapping.get(relation.getTarget().getId());
            result.getMatrix()[i][j] = val;
        });

        return result;
    }

    private Double weight(String username) {
        return 1.0;
    }

}
